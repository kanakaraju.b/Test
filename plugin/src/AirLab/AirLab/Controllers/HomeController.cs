﻿using AirLab.Models;
using KafkaNet;
using KafkaNet.Model;
using KafkaNet.Protocol;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace AirLab.Controllers
{
    public class HomeController : Controller
    {
        private string URL = ConfigurationManager.AppSettings["AirLabURL"].ToString();
        private string StarsURL = ConfigurationManager.AppSettings["AirLabStarsURL"].ToString();
        private string apikey = ConfigurationManager.AppSettings["Apikey"].ToString();
        List<RootAirportsDetails> airportdetails;

        public void GetWayPointDetails(string URL, string AirportName, out JsonResponse jsonResponse, string Type)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            client.DefaultRequestHeaders.Add("api-key", apikey);
            jsonResponse = new JsonResponse();

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync(URL).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;

                var myDeserializedClass = JsonConvert.DeserializeObject<List<RootAirportsDetails>>(content);
                airportdetails = new List<RootAirportsDetails>();

                List<RootAirportsDetails.Waypoint> lstwaypoints = new List<RootAirportsDetails.Waypoint>();

                foreach (var item in myDeserializedClass)
                {
                    foreach (var waypnt in item.waypoints)
                    {
                        lstwaypoints.Add(waypnt);
                    }
                    airportdetails.Add(item);
                }

                var waypointnames = from item in lstwaypoints
                                    group item.name by item.name into grpname
                                    orderby grpname.Count() descending
                                    select new { Name = grpname.Key, Count = grpname.Count() };

                List<JsonWayPoints> lstJsonwaypoint = new List<JsonWayPoints>();
                var lstwaypoint = waypointnames.Take(2);


                foreach (var item in lstwaypoint)
                {
                    JsonWayPoints wayPoints = new JsonWayPoints();

                    wayPoints.waypointName = item.Name;
                    wayPoints.Count = item.Count;
                    lstJsonwaypoint.Add(wayPoints);

                    jsonResponse.Type = Type;
                    jsonResponse.AirPortName = AirportName;
                    jsonResponse.JsonWayPoint = lstJsonwaypoint;
                }

            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }

            // Make any other calls using HttpClient here.

            // Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
            client.Dispose();
        }

        //public JsonResult Index()
        public ViewResult Index()
        {
            List<JsonResponse> lstJsonResPonse = new List<JsonResponse>();

            lstJsonResPonse = InvokeAPI(lstJsonResPonse);

            string payload = new JavaScriptSerializer().Serialize(lstJsonResPonse);
            string topic = "test";
            Message msg = new Message(DateTime.Now + "------------------------" + payload);
            Uri uri = new Uri("http://localhost:9092");
            var options = new KafkaOptions(uri);
            var router = new BrokerRouter(options);
            var client = new Producer(router);
            client.SendMessageAsync(topic, new List<Message> { msg });

            return View(lstJsonResPonse);
            //return Json(lstJsonResPonse, JsonRequestBehavior.AllowGet);
        }

        public List<JsonResponse> InvokeAPI(List<JsonResponse> lstJsonResPonse)
        {
            JsonResponse jsonSIDResponse;
            JsonResponse jsonStarsResponse;

            GetWayPointDetails(URL + "WSSS", "WSSS", out jsonSIDResponse, "SID");
            lstJsonResPonse.Add(jsonSIDResponse);
            GetWayPointDetails(URL + "WSSL", "WSSL", out jsonSIDResponse, "SID");
            lstJsonResPonse.Add(jsonSIDResponse);

            GetWayPointDetails(StarsURL + "WSSS", "WSSS", out jsonStarsResponse, "STARS");
            lstJsonResPonse.Add(jsonStarsResponse);
            GetWayPointDetails(StarsURL + "WSSL", "WSSL", out jsonStarsResponse, "STARS");
            lstJsonResPonse.Add(jsonStarsResponse);
            return lstJsonResPonse;
        }

        [HttpPost]
        public ActionResult Index(string id)
        {
            List<JsonResponse> lstJsonResPonse = new List<JsonResponse>();

            InvokeAPI(lstJsonResPonse);

            string type = Request.Form["Types"].ToString();
            string Airport = Request.Form["AirPortNames"];

            IEnumerable<JsonResponse> jsonresult = lstJsonResPonse.Where(m => m.AirPortName == Airport && m.Type == type);
            ViewBag.JSONRespone = jsonresult;
            LogData(type, Airport, jsonresult);
            return View(lstJsonResPonse);
        }
        public void LogData(string type, string Airport, IEnumerable<JsonResponse> jsonresponse)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ConnStringDb"].ToString();
            using (SqlConnection connection =
            new SqlConnection(connectionString))
            {
                string queryString =
            "INSERT INTO AIRLAB VALUES(@Type,@Airport,@JSONWayPoints,@Date)";

                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@Type", type);
                command.Parameters.AddWithValue("@Airport", Airport);
                command.Parameters.AddWithValue("@JSONWayPoints", new JavaScriptSerializer().Serialize(jsonresponse));
                command.Parameters.AddWithValue("@Date", DateTime.Now);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}