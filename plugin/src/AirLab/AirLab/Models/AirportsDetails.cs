﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirLab.Models
{
    public class RootAirportsDetails
    {
        public string name { get; set; }
        public Airport airport { get; set; }
        public List<Waypoint> waypoints { get; set; }

        public class Airport
        {
            public string uid { get; set; }
            public string name { get; set; }
            public double lat { get; set; }
            public double lng { get; set; }
        }

        public class Waypoint
        {
            public string uid { get; set; }
            public string name { get; set; }
            public double lat { get; set; }
            public double lng { get; set; }
        }
    }
}