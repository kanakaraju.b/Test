﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirLab.Models
{
    public class JsonResponse
    {
        public string Type;
        public string AirPortName;
        public List<JsonWayPoints> JsonWayPoint;
    }

    public class JsonWayPoints
    {
        public string waypointName;
        public int Count;
    }
}